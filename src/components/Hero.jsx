import React from 'react';

import HeroFooter from '../img/hero.svg';
import Memoji from '../img/memoji.svg';

const Hero = () => (
    <div className="hero">
        <div className="hero__body">
            <h1 className="hero__title">Desenvolvedor Full-Stack &amp; Analista de Dados</h1>

            <p className="hero__subtitle">
                Desenvolvo &amp; construo aplicativos móveis, web e desktop
            </p>
            <img src={Memoji} alt="" className="hero__memoji" />

            <div className="hero__footer">
                <img src={HeroFooter} className="hero__footer-img" alt="" />
            </div>
        </div>
    </div>
);

export default React.memo(Hero);
