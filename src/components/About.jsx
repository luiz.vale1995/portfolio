import React from 'react';

import Design from '../img/computer.svg';
import DataApp from '../img/data-science.svg';
import WebApp from '../img/development.svg';
const About = () => {
    const description =
        'Sou um desenvolvedor full stack com 2 anos de experiência no desenvolvimento de aplicativos móveis e web; e construção de aplicativos de desktop';

    const renderBreakdowns = () => {
        const breakdowns = [
            {
                img: Design,
                title: 'Desenvolvimento Frontend',
                description:
                    'Adoro trabalhar no desenvolvimento de wireframes e protótipos de modelos, sempre pensando na UX',
                subTitle: 'Coisas com as quais trabalho',
                subDesc: 'Wireframes, Websites, Protótipos, Aplicativos Híbridos',
                listTitle: 'Ferramentas + Frameworks',
                list: [
                    'ReactJS',
                    'React Native',
                    'Vanilla JS',
                    'JQuery',
                    'Bootstrap',
                    'Compiladores de CSS',
                    'Adobe XD'
                ]
            },
            {
                img: WebApp,
                title: 'Desenvolvimento Backend',
                description:
                    'Eu também adoro construir APIs; servidores websocket e aplicativos de back-end',
                subTitle: 'Coisas com as quais trabalho',
                subDesc: 'Aplicações Web, APIs, Administração de banco de dados',
                listTitle: 'Ferramentas + Linguagens',
                list: [
                    'NodeJS',
                    'Python',
                    'Google Cloud Platform',
                    'Amazon Web Services',
                    'Banco de dados',
                    'Heroku'
                ]
            },
            {
                img: DataApp,
                title: 'Ciência de Dados',
                description:
                    'Mantenho uma enorme paixão por Análise de Dados, Machine Learning e Testes Estátísticos',
                subTitle: 'Coisas com as quais trabalho',
                subDesc:
                    'Algoritmos Supervisionados e não Supervisionados, Mineração de dados, Processamento de Linguagem Natural',
                listTitle: 'Conhecimentos + Ferramentas',
                list: [
                    'Python',
                    'R',
                    'Amazon Web Services',
                    'Testes A/B',
                    'Análise Multivariada de Dados'
                ]
            }
        ];

        return breakdowns.map(breakdown => (
            <div className="about__breakdown col" key={breakdown.title.toLowerCase()}>
                <img
                    data-sal="slide-down"
                    data-sal-delay="300"
                    src={breakdown.img}
                    alt={breakdown.title}
                    className="about__breakdown-img"
                />

                <h3 data-sal="slide-down" data-sal-delay="300" className="about__breakdown-title">
                    {breakdown.title}
                </h3>
                <p data-sal="slide-down" data-sal-delay="300" className="about__breakdown-desc">
                    {breakdown.description}
                </p>

                <p data-sal="slide-down" data-sal-delay="300" className="about__breakdown-subtitle">
                    {breakdown.subTitle}
                </p>
                <p data-sal="slide-down" data-sal-delay="300" className="about__breakdown-subdesc">
                    {breakdown.subDesc}
                </p>

                <p data-sal="slide-down" data-sal-delay="300" className="about__breakdown-subtitle">
                    {breakdown.listTitle}
                </p>
                <ul className="about__breakdown-list">
                    {breakdown.list.map(elem => (
                        <li
                            data-sal="slide-down"
                            data-sal-delay="300"
                            key={elem}
                            className="about__breakdown-elem">
                            {elem}
                        </li>
                    ))}
                </ul>
            </div>
        ));
    };

    return (
        <div className="about">
            <div className="about__inner">
                <h2 className="about__title" data-sal="slide-down" data-sal-delay="300">
                    Olá ! Meu nome é Luiz Felipe. É um prazer te conhecer 👋🏼
                </h2>
                <p className="about__text" data-sal="slide-down" data-sal-delay="300">
                    {description}
                </p>

                <a href="/Curriculo.pdf" download="" className="about__cv">
                    Baixar meu currículo &nbsp;
                    <i className="ti-file" />
                </a>
            </div>

            <div className="about__breakdowns grid grid--padded">{renderBreakdowns()}</div>
        </div>
    );
};

export default React.memo(About);
