import React from 'react';

import ContactForm from './ContactForm';

const Contact = () => (
    <div className="contact   grid grid--padded">
        <h2 data-sal="slide-down" data-sal-delay="300" className="contact__title">
            Entre em contato ou me envie alguma sugestão
        </h2>

        <p data-sal="slide-down" data-sal-delay="300" className="contact__subtitle">
            Eu estou sempre aberto para discutir novas ideias
        </p>

        <ContactForm />
    </div>
);

export default Contact;
